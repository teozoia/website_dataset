import os
import json
import matplotlib.pyplot as plt

json_directory = "./it_top1000"
website_count = {}

# Iterate through the JSON files in the directory
for filename in os.listdir(json_directory):
    if filename.endswith(".json"):
        with open(os.path.join(json_directory, filename), 'r') as file:
            data = json.load(file)

            # Normalize the website URL
            website_url = data["website"]

            for x in data.get("cookie1", []):
                # Check if the website_url is already in the dictionary
                if website_url in website_count:
                    website_count[website_url] += 1
                else:
                    website_count[website_url] = 1

# Sort the URLs by count in descending order
sorted_urls = sorted(website_count.items(), key=lambda x: x[1], reverse=True)

# Select only the top 20 URLs and their counts
top_20_urls = sorted_urls[:20]

# Create lists for URLs and their counts
urls, counts = zip(*top_20_urls)

# Create a histogram plot for the top 20 URLs
plt.figure(figsize=(12, 6))
plt.bar(range(len(urls)), counts, tick_label=urls)
plt.xlabel("Website URLs")
plt.ylabel("Counts")
plt.xticks(rotation=45)
plt.title("Top 20 Website URL Counts in JSON Files")
plt.tight_layout()

# Show the plot
plt.savefig("topcookie.pdf")
