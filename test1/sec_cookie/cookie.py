import os
import json
import matplotlib.pyplot as plt
import numpy as np

# Definisci un dizionario per tenere traccia dei conteggi
conteggi = {
    "cookie1_e_cookie2_maggiori_di_zero": 0,
    "solo_cookie1_maggiore_di_zero": 0,
    "media_cookie1": 0,
    "media_cookie2": 0,
    "count_cookie1": 0,
    "count_cookie2": 0
}

# Definisci un dizionario per i conteggi dei nomi dei cookie con il rispettivo dominio
cookie_info = {}

# Directory dei file JSON
directory = "./it_top1000/"

# Cerca i file .json nella directory
for filename in os.listdir(directory):
    if filename.endswith(".json"):
        with open(os.path.join(directory, filename), 'r', encoding='utf-8') as file:
            data = json.load(file)

            # Estrai le informazioni sul sito web
            website = data["website"]
            cookie1 = data.get("cookie1", [])
            cookie2 = data.get("cookie2", [])

            if len(cookie1) > 0 and len(cookie2) > 0 and len(cookie2) >= len(cookie1):
                print(f"Website: {website}")
                print(f"Cookie1: {len(cookie1)} elementi")
                print(f"Cookie2: {len(cookie2)} elementi")
                conteggi["cookie1_e_cookie2_maggiori_di_zero"] += 1
                conteggi["count_cookie1"] += 1
                conteggi["count_cookie2"] += 1
            elif len(cookie1) > 0:
                print(f"Website: {website}")
                print(f"Cookie1-only: {len(cookie1)} elementi")
                conteggi["solo_cookie1_maggiore_di_zero"] += 1
                conteggi["count_cookie1"] += 1

            # Aggiorna la media
            conteggi["media_cookie1"] += len(cookie1)
            conteggi["media_cookie2"] += len(cookie2)

            # Estrai i nomi dei cookie in "cookie1" e il dominio associato, quindi aggiorna il conteggio
            if len(cookie1) > 0:
                for c in cookie1:
                    nome_cookie = c.get("name")
                    domain_cookie = c.get("domain")
                    is_third_party = domain_cookie[4:] not in website
                    cookie_info.setdefault(nome_cookie, {})
                    cookie_info[nome_cookie].setdefault("domains", [])
                    cookie_info[nome_cookie]["domains"].append(
                        {
                            "website": website,
                            "is_third": is_third_party,
                            "cookie_domain": domain_cookie
                        }
                    )

conteggi["media_cookie1"] /= conteggi["count_cookie1"]
conteggi["media_cookie2"] /= conteggi["count_cookie2"]

# Stampa il totale dei siti web analizzati in ciascuna categoria e le medie
print("Totale siti web con Cookie1 e Cookie2 maggiori di zero:", conteggi["cookie1_e_cookie2_maggiori_di_zero"])
print("Totale siti web con Cookie1 maggiori di zero:", conteggi["solo_cookie1_maggiore_di_zero"])
print("Media di Cookie1:", conteggi["media_cookie1"])
print("Media di Cookie2:", conteggi["media_cookie2"])

cookie = []
count_array = []
third_count_array = []

for c in cookie_info:
    if len(cookie_info[c]['domains']) > 35:
        print(f"cookie: {c}, domain_count: {len(cookie_info[c]['domains'])}")
        cookie.append(c)

        third_count = 0
        for is_third in cookie_info[c]['domains']:
            if is_third["is_third"]:
                third_count += 1
        print(f"\t3rd party count: {third_count}")

        count_array.append(len(cookie_info[c]['domains']))
        third_count_array.append(third_count)



# Get the indices that would sort the count_array
sorted_indices = np.argsort(count_array)

# Sort count_array
sorted_count_array = [count_array[i] for i in sorted_indices]

# Sort the other arrays based on the sorted indices
sorted_cookie = [cookie[i] for i in sorted_indices]
sorted_third_count_array = [third_count_array[i] for i in sorted_indices]

sorted_count_array.reverse()
sorted_cookie.reverse()
sorted_third_count_array.reverse()


# Create a stacked plot
fig, ax = plt.subplots(figsize=(8, 6))

# Plot the first category
plt.bar(sorted_cookie, sorted_count_array, label='Same domain', alpha=0.7)

# Plot the second category on top of the first
plt.bar(sorted_cookie, sorted_third_count_array, label='3rd party cookie', bottom=sorted_count_array, alpha=0.7)


# Customize the plot
plt.xlabel('Cookie')
plt.ylabel('Count')
plt.title('3rd party cookie (>35)')
plt.xticks(cookie, rotation=45, ha='right')
fig.subplots_adjust(bottom=0.3)

plt.legend()

# Show the plot
plt.savefig('stacked_plot.pdf')

print(sorted_cookie)

