import os
import json
from urllib.parse import urlparse
import matplotlib.pyplot as plt
import networkx as nx


json_directory = "./it_top1000"
count = 0

# Iterate through the JSON files in the directory
for filename in os.listdir(json_directory):
    if filename.endswith("it.json"):
        with open(os.path.join(json_directory, filename), 'r') as file:
            data = json.load(file)

            # Normalize the website URL
            website_url = data["website"]
            pref = data.get("cookie_preferences", [])

            if len(pref) > 0:
                count += 1

print(count)
