import os
import json
from urllib.parse import urlparse
import matplotlib.pyplot as plt
import networkx as nx

# Create a networkx graph to represent the relationships
G = nx.Graph()

json_directory = "./it_top1000"


# Iterate through the JSON files in the directory
for filename in os.listdir(json_directory):
    if filename.endswith("it.json"):
        with open(os.path.join(json_directory, filename), 'r') as file:
            data = json.load(file)

            # Normalize the website URL
            website_url = data["website"]
            if website_url.startswith('https://'):
                website_url = website_url[8:]
            if website_url.startswith('http://'):
                website_url = website_url[7:]
            if website_url.startswith('.'):
                website_url = website_url[1:]
            if website_url.startswith('www.'):
                website_url = website_url[4:]

            link_set = set()

            # Iterate through the cookies and normalize their domains
            for cookie in data["cookie1"]:
                cookie_domain = cookie["domain"]
                if cookie_domain.startswith('.'):
                    cookie_domain = cookie_domain[1:]
                if cookie_domain.startswith('www.'):
                    cookie_domain = cookie_domain[4:]

                link_set.add(cookie_domain)

            link_set.discard(website_url)
            for c in link_set:
                G.add_edge(website_url, c)

nx.write_gml(G, 'network.nx') 
# Calculate the degree of each node (number of edges)
node_degrees = dict(G.degree)

# Define a scaling factor for node size
scale_factor = 100

# Define different colors for nodes
node_sizes = [scale_factor * (node_degrees[node] + 1) for node in G.nodes]

# Draw the graph with enlarged nodes and different colors
pos = nx.kamada_kawai_layout(G)  # Position nodes using a spring layout
nx.draw(G, pos, with_labels=True, node_size=node_sizes, node_color='lightblue', font_size=8, font_color='black')
plt.axis('off')

plt.savefig('map.pdf')
